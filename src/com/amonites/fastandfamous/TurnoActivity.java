package com.amonites.fastandfamous;

import java.util.ArrayList;

import com.amonites.clases.CartaJugada;
import com.amonites.clases.CartasViewAdapter;
import com.amonites.clases.Mazo.MazoRonda;
import com.amonites.clases.Partida;
import com.amonites.clases.RevisionCartasAdapter;
import com.amonites.elementosGraficos.CartasView;
import com.amonites.elementosGraficos.CartasView.AccionCartas;
import com.amonites.excepciones.CantUseFirstCardException;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ViewFlipper;

public class TurnoActivity extends Activity {

	//protected static final float SWIPE_DURATION = 250;

	private static int INFO_INI = 0;
	private static int CUENTA_ATRAS = 1;
	private static int MUESTRA_CARTAS = 2;
	private static int REVISION = 3;


	private ViewFlipper vf;
	private TextView tvContador;
	TextView pareja;
	private TurnoActivity context;
	private ListView lista;
	private CountDownTimer contador;
	private ProgressBar barra_progreso;
	private ArrayList<CartaJugada> cartas_jugadas;
	private MazoRonda mazo_cartas; 
	private CartasView cartasView;
	private int estado;
	//private TextView nombre_personaje;
	static private int TIEMPO_CUENTA_ATRAS = 1500;
	boolean mAnimating = false;
	//private boolean mSwiping;
	//para la prueba
	private long tiempo_turno = 10000;
	private Partida partida;
	private long tiempo_restante;
	private TextView tvContinua;

	//private View carta;


	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_turno);
		vf = (ViewFlipper) findViewById(R.id.viewFlipper);
		barra_progreso = (ProgressBar) findViewById(R.id.progreso_tiempo_turno);
		tvContador = (TextView) findViewById(R.id.contador_inicial);
		tvContinua = (TextView) findViewById(R.id.texto_turno_pareja);
		lista = (ListView) findViewById(R.id.lista_revision_cartas);
		pareja = (TextView) findViewById(R.id.num_turno_pareja);
		cartasView = (CartasView) findViewById(R.id.cartaFlip);
		context = this;
		if (savedInstanceState==null){
			cartas_jugadas = new ArrayList<CartaJugada>();
			partida = (Partida)getIntent().getSerializableExtra("Partida");
			tiempo_restante = partida.getRondaActual().time*1000;
			partida.siguienteTurno();
		} else {//si es una sesion recuperada
			partida = (Partida) savedInstanceState.get("partida");
			cartas_jugadas = (ArrayList<CartaJugada>) savedInstanceState.get("cartas_jugadas");
			tiempo_restante = savedInstanceState.getLong("tiempo_restante");
			estado = savedInstanceState.getInt("estado");
		}
		prepareTurno();
	}

	private void prepareTurno(){
		tiempo_turno = partida.getRondaActual().time*1000;
		mazo_cartas = partida.getMazoActual();
		CartasViewAdapter adaptador = new CartasViewAdapter(this, R.id.carta, mazo_cartas, new AccionCartas(){

			@Override
			public void cartaALaIzquierda() {
				personajePasado();

			}

			@Override
			public void cartaALaDerecha() {
				personajeAcertado();
			}

		});
		cartasView.setAdapter(adaptador);

		lista.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				cartas_jugadas.get(arg2).acertada=!cartas_jugadas.get(arg2).acertada;
				((RevisionCartasAdapter)arg0.getAdapter()).notifyDataSetChanged();

			}

		});
		pareja.setText(partida.getPareja_actual()+"");
	}
	
	private void prepareCounter(){
		contador = new CountDownTimer(tiempo_restante+5,50){
			@Override
			public void onFinish() {
				cartaJugada(false);
				try {
					mazo_cartas.pasarCarta();
				} catch (CantUseFirstCardException e) {
				}
				pasarARevision();
			}

			@Override
			public void onTick(long millisUntilFinished) {
				barra_progreso.setProgress((int) ((millisUntilFinished*100)/tiempo_turno));
				tiempo_restante = millisUntilFinished;
			}
		};
		vf.setDisplayedChild(INFO_INI);
		estado=INFO_INI;
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (estado!=REVISION){
			contador.cancel();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (estado==REVISION){
			pasarARevision();
		} else {
			if (estado!=INFO_INI){
				tvContinua.setText(R.string.continue_turn_text);
			}
			prepareCounter();
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putSerializable("partida", partida);
		outState.putSerializable("cartas_jugadas", cartas_jugadas);
		outState.putInt("estado", estado);
		if (vf.getDisplayedChild()==REVISION){
			outState.putLong("tiempo_restante", -1);
		}else{
			outState.putLong("tiempo_restante", tiempo_restante);
		}
		super.onSaveInstanceState(outState);
	}

	/**
	 * Cuenta atras. Se inicia al tocar la pantalla
	 *
	 * @param view the view
	 */
	public void cuentaAtras(View view){
		tvContador.setText("3");
		estado=CUENTA_ATRAS;
		final int tick = TIEMPO_CUENTA_ATRAS/3;
		new CountDownTimer(TIEMPO_CUENTA_ATRAS, tick/100) {
			private int contadornum = 3;
			private float anterior = 0f;
			private Animation animacionConta = AnimationUtils.loadAnimation(context, R.anim.cronometro_inicio);
			public void onTick(long millisUntilFinished) {
				if (millisUntilFinished%tick>anterior){
					tvContador.setText(contadornum+"");
					contadornum--;
					tvContador.startAnimation(animacionConta);
				}
				anterior=millisUntilFinished%tick;
			}

			public void onFinish() {
				vf.setDisplayedChild(MUESTRA_CARTAS);
				contador.start();
				estado=MUESTRA_CARTAS;
			}
		}.start();
		vf.setDisplayedChild(CUENTA_ATRAS);
	}

	public void personajeAcertado(){
		cartaJugada(true);
		try{
			cartasView.showNextCard();
		} catch (CantUseFirstCardException e) {
			contador.cancel();
			pasarARevision();
		}
	}

	public void personajePasado(){
		cartaJugada(false);
		try{
			cartasView.showNextCard();
		} catch (CantUseFirstCardException e) {
			contador.cancel();
			pasarARevision();
		}
	}

	public void finalizarTurno(View view){
		int conta = 0;
		for (CartaJugada c:cartas_jugadas){
			if (c.acertada){
				mazo_cartas.eliminarCarta(c.carta);
				conta++;
			}
		}

		partida.getScoreboard().addPuntuacion(conta, partida.getNumero_ronda_actual(), partida.getPareja_actual());

		Intent actividad = new Intent (TurnoActivity.this, RondaActivity.class);
		Bundle b = new Bundle();
		b.putSerializable("Partida", partida);
		actividad.putExtras(b);

		setResult(Activity.RESULT_OK, actividad);
		finish();
	}

	public void cartaJugada(boolean estado){
		CartaJugada carta = new CartaJugada();
		carta.acertada=estado;
		carta.carta=mazo_cartas.getCarta_actual();
		cartas_jugadas.add(carta);
	}

	public void pasarARevision(){
		lista.setAdapter(new RevisionCartasAdapter(context, R.layout.elemento_revision, cartas_jugadas));
		vf.setDisplayedChild(REVISION);
		estado=REVISION;
	}

}
