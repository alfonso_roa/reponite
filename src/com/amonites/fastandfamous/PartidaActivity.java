package com.amonites.fastandfamous;

import java.util.ArrayList;

import com.amonites.clases.Mazo;
import com.amonites.clases.Partida;
import com.amonites.clases.Scoreboard;
import com.amonites.excepciones.NotEnoughCardsException;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class PartidaActivity extends Activity {

	private static final int RONDA = 0;
	private static final int CONFIG = 1;
	private Partida partida;
	TableLayout tabla;
	TableRow cabeza;
	private ArrayList<Mazo> mazos;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		logpartida("entro en partida");
		setContentView(R.layout.activity_partida);
		mazos = (ArrayList<Mazo>)getIntent().getSerializableExtra("mazos");
		tabla = (TableLayout)findViewById(R.id.tabla_partida);
		cabeza = (TableRow) findViewById(R.id.cabeza_partida);
		if (savedInstanceState==null){
			lanzaConfiguracion();
		} else {
			partida = (Partida) savedInstanceState.get("partida");
			creaTabla();
		}
		logpartida(R.string.class.getDeclaredFields()[0].getName());
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.partida, menu);
		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode==PartidaActivity.RONDA && resultCode==Activity.RESULT_OK){
			partida = (Partida) data.getSerializableExtra("Partida");
			if (partida.isLastRound()){
				logpartida("creo la tabla porque termin� del todo");
				creaTabla();
			} else {
				logpartida("creo otra ronda porque quedan");
				lanzaRonda();
			}
		}
		logpartida(requestCode+" "+PartidaActivity.CONFIG+" "+requestCode+" "+Activity.RESULT_CANCELED);
		if (requestCode==PartidaActivity.CONFIG){
			logpartida("resultado de la configuracion");
			if (resultCode==Activity.RESULT_OK){
				partida = (Partida) data.getSerializableExtra("Partida");
				try {
				partida.iniciarPartida();//inicializacion de la partida
			} catch (NotEnoughCardsException e) {
				finish();
			};
				logpartida("tengo la configuracion y lanzo la primera ronda");
				lanzaRonda();
			} else if (resultCode==Activity.RESULT_CANCELED){
				logpartida("cancelaron la configuracion");
				finish();
			}
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putSerializable("partida", partida);
		super.onSaveInstanceState(outState);
	}


	public void finalizarPartida(View view){
		finish();
	}

	private void lanzaConfiguracion(){
		Intent actividad = new Intent (PartidaActivity.this, ConfActivity.class);
		Bundle b = new Bundle();
		b.putSerializable("mazos", mazos);
		actividad.putExtras(b);
		startActivityForResult(actividad, PartidaActivity.CONFIG);
	}

	private void creaTabla(){
		for (int i=0; i<=partida.getNumero_ronda_actual();i++){
			TextView ron = new TextView(this);
			ron.setText(i+1+"");
			cabeza.addView(ron);
		}
		TextView tot = new TextView(this);
		tot.setText("Total");
		cabeza.addView(tot);

		Scoreboard score = partida.getScoreboard();

		for(int i=0; i<partida.getNum_pareja(); i++){
			TableRow pareja = new TableRow(this);
			TextView pare = new TextView(this);
			pare.setText("Pareja "+(i+1));
			pareja.addView(pare);
			for (int j=0; j<=partida.getNumero_ronda_actual(); j++){
				TextView ron = new TextView(this);
				ron.setText(score.getPuntuacion(j, i)+"");
				pareja.addView(ron);
			}
			TextView total = new TextView(this);
			total.setText(score.getPuntuacionTotal(i)+"");
			pareja.addView(total);
			tabla.addView(pareja);
		}
	}

	private void lanzaRonda(){
		Intent actividad = new Intent (PartidaActivity.this, RondaActivity.class);
		Bundle b = new Bundle();
		b.putSerializable("Partida", partida);
		actividad.putExtras(b);
		startActivityForResult(actividad, PartidaActivity.RONDA);
	}

	private void logpartida(String tex){
		Log.i("partida", tex);
	}
}
