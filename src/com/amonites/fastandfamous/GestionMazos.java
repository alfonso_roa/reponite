package com.amonites.fastandfamous;

import java.util.ArrayList;

import com.amonites.adapters.MazosAdapter;
import com.amonites.clases.Carta;
import com.amonites.clases.Mazo;
import com.amonites.daos.MazosDAO;
import com.amonites.elementosGraficos.CheckableRelativeLayout;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewFlipper;

public class GestionMazos extends Activity {

	private ViewFlipper vf;
	private ArrayList<Mazo> mazos;
	private ListView lista_mazos_gestion;
	private ListView lista_cartas_gestion;
	private Context context;
	private MazosDAO mdao;
	private ArrayAdapter<Mazo> dataMazos;

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gestion_mazos);
		context = this;
		
		mdao = new MazosDAO(context);
		mdao.open();

		mazos = (ArrayList<Mazo>)getIntent().getSerializableExtra("mazos");


		vf = (ViewFlipper) findViewById(R.id.viewFlipper2);
		lista_mazos_gestion = (ListView) findViewById(R.id.lista_mazos_gestion);
		lista_cartas_gestion = (ListView) findViewById(R.id.lista_cartas_gestion);

		dataMazos=new MazosAdapter(this, R.layout.elemento_mazo, mazos);
		//Le asigno ese adapter a la lista
		lista_mazos_gestion.setAdapter(dataMazos);

	}

	public void toggleListViewMazos(View view){
		CheckableRelativeLayout ll = (CheckableRelativeLayout)view;
		TextView tv = (TextView)ll.getChildAt(0);
		Integer item_int = (Integer) tv.getTag();
		ArrayList<Carta> cartas_mazo = mazos.get(item_int).getCartas();	

		lista_cartas_gestion = (ListView) findViewById(R.id.lista_cartas_gestion);
		ArrayAdapter<Carta> data= new ArrayAdapter<Carta>(context, android.R.layout.simple_list_item_1, cartas_mazo);
		lista_cartas_gestion.setAdapter(data);

		vf.showNext();
	}


	public void addNewCollection(View view){
		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle("Title");
		alert.setMessage("Message");

		// Set an EditText view to get user input 
		final EditText input = new EditText(this);
		alert.setView(input);

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				Editable value = input.getText();
				Mazo mazo = new Mazo(value.toString(), 0, false, new ArrayList<Carta>());
				mdao.aniadirMazo(mazo);
				mazos.add(mazo);
				dataMazos.notifyDataSetChanged();
			}
		});

		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				// Canceled.
			}
		});

		alert.show();
	}

	@Override
	public void onBackPressed() {
		if (vf.getDisplayedChild() == 1){
			vf.showPrevious();
		}
		else{
			finish();
		}
	}

	/*@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if ((keyCode == KeyEvent.KEYCODE_BACK)) { //Back key pressed

	    	if (vf.getDisplayedChild() == 1){
	    		vf.showPrevious();
	    	}
	    	else{
	    		finish();
	    	}

	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}*/

}
