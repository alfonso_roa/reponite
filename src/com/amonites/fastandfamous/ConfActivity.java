package com.amonites.fastandfamous;

import java.util.ArrayList;

import com.amonites.adapters.MazosAdapter;
import com.amonites.adapters.ModosRondaAdapter;
import com.amonites.clases.Carta;
import com.amonites.clases.Mazo;
import com.amonites.clases.Partida;
import com.amonites.clases.Ronda;
import com.amonites.clases.RondaBasica;
import com.amonites.clases.RondaMimica;
import com.amonites.clases.RondaPalabro;
import com.amonites.elementosGraficos.CheckableRelativeLayout;
import com.amonites.excepciones.NotEnoughCardsException;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

public class ConfActivity extends Activity {

	private Partida partida;
	//confMazo
	private ListView lista_mazos;
	private TextView num_cartas;
	private ArrayList<Mazo> mazos_partida;

	//confRondas
	private ListView lista_modos;
	private ArrayList<Ronda> tipos_ronda = new ArrayList<Ronda>();

	private boolean preparado = false;

	//confParejas
	//private int count;
	private TextView parejas;

	private ViewFlipper vf;
	
	private Button boton_start;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_conf);
		mazos_partida = (ArrayList<Mazo>)getIntent().getSerializableExtra("mazos");
		boton_start = (Button) findViewById(R.id.boton_aceptar_configuracion);
		if (savedInstanceState==null){
			partida = new Partida();
		} else {
			partida = (Partida) savedInstanceState.get("partida");
		}
		vf = (ViewFlipper) findViewById(R.id.vfConfig);

		//confMazo
		lista_mazos = (ListView) findViewById(R.id.lista_mazos);
		lista_mazos.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);

		
		num_cartas = (TextView) findViewById(R.id.num_mazos); 
		//se actualiza en el interfaz directamente el numero, para que no salte de 0 a 3 y partida sepa el n�mero mostrado desde el principio
		num_cartas.setText(((Integer)partida.getNumCartas()).toString());
		
		//TODO aqui no se tendr�an que crear los mazos si no importarlos de la bd
		//TODO eliminar la creacion de rondas en caliente y sacarlo de la base de datos
		//mazos_partida = this.creaMazos();

		ArrayAdapter<Mazo> data=new MazosAdapter(this, R.layout.elemento_mazo, mazos_partida);
		//Le asigno ese adapter a la lista
		lista_mazos.setAdapter(data);

		//confRondas
		//deberiamos seguir una nomeclatura para los elementos, si no va a ser un follon.
		lista_modos = (ListView)findViewById(R.id.lista_modos);
		lista_modos.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
		final ArrayAdapter<Ronda> adapter = new ModosRondaAdapter(this,R.layout.elemento_ronda, tipos_ronda);

		cargaRondas();

		lista_modos.setAdapter(adapter);

		lista_modos.setItemChecked(0, true);//selecciona de forma obligatoria el primer elemento (ha de ser la basica)		


		//confParejas
		parejas = (TextView) findViewById(R.id.num_parejas); 

		//se actualiza en el interfaz directamente el numero, para que no salte de 0 a 3 y partida sepa el n�mero mostrado desde el principio
		parejas.setText(((Integer)partida.getNum_pareja()).toString());
		updateRondas();
		setupStartButton();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.conf, menu);
		return true;
	}


	//confMazo
	private void updateMazos() {
		preparado = true;
		//Obtengo los elementos seleccionados de mi lista
		SparseBooleanArray seleccionados = lista_mazos.getCheckedItemPositions();

		if(seleccionados==null || seleccionados.size()==0){
			//Si no hab�a elementos seleccionados...
			Toast.makeText(this,"No hay mazos seleccionados",Toast.LENGTH_SHORT).show();
			preparado = false;
		}else{
			//si los hab�a, miro sus valores
			//Recorro my "array" de elementos seleccionados
			final int size=seleccionados.size();
			ArrayList<Mazo> mazos_seleccionados = new ArrayList<Mazo>();
			for (int i=0; i<size; i++) {
				//Si valueAt(i) es true, es que estaba seleccionado
				if (seleccionados.valueAt(i)) {
					//con keyAt(i) obtengo su posici�n
					mazos_seleccionados.add(mazos_partida.get(seleccionados.keyAt(i)));
				}
			}
			try {
				partida.setConjunto_mazos(mazos_seleccionados);
			} catch (NotEnoughCardsException e) {
				Toast.makeText(this,"No hay cartas suficientes. Reduzca el n�mero de cartas o a�ada mazos",Toast.LENGTH_SHORT).show();
				preparado = false;
			}
		}
		setupStartButton();
	}

	public void sumarCincoCartas (View view){
		preparado = true;
		try {
			partida.setNumCartas(partida.getNumCartas()+5);
		} catch (NotEnoughCardsException e) {
			preparado = false;
		}
		num_cartas.setText(((Integer)partida.getNumCartas()).toString());
		setupStartButton();	

	}

	public void restarCincoCartas (View view){
		preparado = true;
		if (partida.getNumCartas() > 5){
			try {
				partida.setNumCartas(partida.getNumCartas()-5);
			} catch (NotEnoughCardsException e) {
				preparado = false;
			}
			num_cartas.setText(((Integer)partida.getNumCartas()).toString());
			setupStartButton();
		}		
	}

	private void setupStartButton() {
		// TODO Auto-generated method stub
		boton_start.setEnabled(preparado);
		if (preparado){
			boton_start.setAlpha(1);
		}else{
			boton_start.setAlpha(0.5f);
		}
	}

	//confRondas
	private boolean updateRondas() {
		//Obtengo los elementos seleccionados de mi lista

		SparseBooleanArray seleccionados = lista_modos.getCheckedItemPositions();

		if(seleccionados==null || seleccionados.size()==0){
			//Si no hab�a elementos seleccionados...
			Toast.makeText(this,"No hay elementos seleccionados",Toast.LENGTH_SHORT).show();
			return false;
		}else{
			//Recorro my "array" de elementos seleccionados
			final int size=seleccionados.size();
			ArrayList<Ronda> rondas_seleccionadas = new ArrayList<Ronda>();
			for (int i=0; i<size; i++) {
				//Si valueAt(i) es true, es que estaba seleccionado
				if (seleccionados.valueAt(i)) {
					//en keyAt(i) obtengo su posicion
					rondas_seleccionadas.add((Ronda) lista_modos.getItemAtPosition(seleccionados.keyAt(i)));
				}
			}
			partida.setRondas(rondas_seleccionadas);
			return true;
		}
	}

	public void incrementarTiempo(View view){
		Button boton = (Button)view;
		CheckableRelativeLayout ll = (CheckableRelativeLayout)boton.getParent();
		TextView tv = (TextView)ll.getChildAt(0);
		Integer position = (Integer) tv.getTag();
		Ronda ronda = tipos_ronda.get(position);
		ronda.incrementarTiempo();
		ListView linla = (ListView)ll.getParent();
		((ModosRondaAdapter)linla.getAdapter()).notifyDataSetChanged();
	}

	public void decrementarTiempo(View view){
		Button boton = (Button)view;
		CheckableRelativeLayout ll = (CheckableRelativeLayout)boton.getParent();
		TextView tv = (TextView)ll.getChildAt(0);
		Integer position = (Integer) tv.getTag();
		Ronda ronda = tipos_ronda.get(position);
		ronda.decrementarTiempo();
		ListView linla = (ListView)ll.getParent();
		((ModosRondaAdapter)linla.getAdapter()).notifyDataSetChanged();
	}

	public void toggleListViewRondas(View view){
		CheckableRelativeLayout ll = (CheckableRelativeLayout)view;
		TextView tv = (TextView)ll.getChildAt(0);
		Integer position = (Integer) tv.getTag();
		if (position!=0){
			lista_modos.setItemChecked(position, !lista_modos.isItemChecked(position));
		}
		updateRondas();
	}
	
	public void toggleListViewMazos(View view){
		CheckableRelativeLayout ll = (CheckableRelativeLayout)view;
		TextView tv = (TextView)ll.getChildAt(0);
		Integer position = (Integer) tv.getTag();
		lista_mazos.setItemChecked(position, !lista_mazos.isItemChecked(position));
		updateMazos();
	}

	//confParejas
	public void incrementarParejas(View v) {
		partida.setNum_parejas(partida.getNum_pareja()+1);
		parejas.setText(((Integer)partida.getNum_pareja()).toString());
	}

	public void decrementarParejas(View v) {

		if (partida.getNum_pareja() > 0){
			partida.setNum_parejas(partida.getNum_pareja()-1);
			parejas.setText(((Integer)partida.getNum_pareja()).toString());
		}		
	}

	public void devolverConfiguracion(View v){
		Intent actividad = new Intent (ConfActivity.this, PartidaActivity.class);
		Bundle b = new Bundle();
		b.putSerializable("Partida", partida);
		actividad.putExtras(b);

		setResult(Activity.RESULT_OK, actividad);
		finish();
	}

	public void confBotSiguiente(View v){
		vf.showNext();
	}

	public void confBotAnterior(View v){
		vf.showPrevious();
	}

	public void aceptarConfiguracion(View v){
		if (preparado){

			Intent actividad = new Intent (ConfActivity.this, PartidaActivity.class);
			Bundle b = new Bundle();
			b.putSerializable("Partida", partida);
			actividad.putExtras(b);

			setResult(Activity.RESULT_OK, actividad);
			finish();
		}
	}

	@Override
	public void onBackPressed() {
		Intent actividad = new Intent (ConfActivity.this, PartidaActivity.class);
		Log.i("partida","le doy a back");
		Bundle b = new Bundle();
		actividad.putExtras(b);
		setResult(Activity.RESULT_CANCELED, actividad);
		finish();
	}
	
	

	//ESTO LUEGO SALE DE LA BD
	private void cargaRondas(){
		Ronda ronda = new RondaBasica(30, 0);
		tipos_ronda.add(ronda);
		ronda = new RondaPalabro(30, 1);
		tipos_ronda.add(ronda);
		ronda = new RondaMimica(30, 2);
		tipos_ronda.add(ronda);
	}
}
