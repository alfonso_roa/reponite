package com.amonites.fastandfamous;

import com.amonites.clases.Partida;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class RondaActivity extends Activity {

	private static final int TURNO = 0;
	private Partida partida;
	private TableLayout tabla;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ronda);
		tabla = (TableLayout) findViewById(R.id.tabla_ronda);
		if (savedInstanceState==null){
			partida = (Partida)getIntent().getSerializableExtra("Partida");
			partida.iniciarRonda();
			lanzaTurno();
		} else {
			partida = (Partida) savedInstanceState.get("partida");
			creaTabla();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ronda, menu);
		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode==RondaActivity.TURNO && resultCode==Activity.RESULT_OK){
			partida = (Partida) data.getSerializableExtra("Partida");
			if (partida.getMazoActual().quedanCartas()){
				lanzaTurno();
			} else {
				Log.i("yepe", "se acab� la ronda");
				creaTabla();
			}
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putSerializable("partida", partida);
		super.onSaveInstanceState(outState);
	}

	private void lanzaTurno(){
		Intent actividad = new Intent (RondaActivity.this, TurnoActivity.class);
		Bundle b = new Bundle();
		b.putSerializable("Partida", partida);
		actividad.putExtras(b);
		startActivityForResult(actividad, RondaActivity.TURNO);
	}

	private void creaTabla(){
		for (int i=0;i<partida.getNum_pareja();i++){
			TableRow jugador = new TableRow(this);
			TextView nombre = new TextView(this);
			nombre.setText("Pareja "+i);
			jugador.addView(nombre);
			TextView puntuRonda = new TextView(this);
			puntuRonda.setText(""+partida.getScoreboard().getPuntuacion(partida.getNumero_ronda_actual(), i));
			jugador.addView(puntuRonda);
			TextView puntuTotal = new TextView(this);
			puntuTotal.setText(""+partida.getScoreboard().getPuntuacionTotal(i));
			jugador.addView(puntuTotal);
			tabla.addView(jugador);
		}
	}

	public void finalizarRonda(View view){
		Intent actividad = new Intent (RondaActivity.this, PartidaActivity.class);
		Bundle b = new Bundle();
		b.putSerializable("Partida", partida);
		actividad.putExtras(b);

		setResult(Activity.RESULT_OK, actividad);
		finish();
	}
}
