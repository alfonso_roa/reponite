package com.amonites.fastandfamous;

import java.util.ArrayList;

import com.amonites.clases.Mazo;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.content.SharedPreferences;

public class MainActivity extends Activity {

	private ArrayList<Mazo> mazos;
	private CheckBox dontShowAgain;
	
	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		mazos = (ArrayList<Mazo>)getIntent().getSerializableExtra("mazos");
		
		Button b_crear_partida = (Button) findViewById(R.id.b_crear_partida);
		Button b_gestion_mazos = (Button) findViewById(R.id.b_gestion_mazos);
		
		
		
		b_crear_partida.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				Intent actividad = new Intent (MainActivity.this, PartidaActivity.class);
				Bundle b = new Bundle();
				b.putSerializable("mazos", mazos);
				actividad.putExtras(b);
				startActivity(actividad);
			}
		});
		
		b_gestion_mazos.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				Intent actividad = new Intent (MainActivity.this, GestionMazos.class);
				Bundle b = new Bundle();
				b.putSerializable("mazos", mazos);
				actividad.putExtras(b);
				startActivity(actividad);
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	
	
	@Override
    protected void onResume() {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        LayoutInflater adbInflater = LayoutInflater.from(this);
        View eulaLayout = adbInflater.inflate(R.layout.don_show_again_layout, null);
        dontShowAgain = (CheckBox) eulaLayout.findViewById(R.id.skip);
        adb.setView(eulaLayout);
        adb.setTitle("Aceptar/Rechazar Cartas");
        adb.setMessage(Html.fromHtml("Desliza la carta hacia la derecha si tu compñero acierta, hacia la izquierda si la falla"));
        adb.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (dontShowAgain.isChecked()){
                	SharedPreferences settings = getSharedPreferences("prefs", 0);
                    settings.edit().putBoolean("dontshow", true).commit();
                }
                return;
            }
        });

        adb.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                /*String checkBoxResult = "NOT checked";
                if (dontShowAgain.isChecked())
                    checkBoxResult = "checked";
                SharedPreferences settings = getSharedPreferences("prefs", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("skipMessage", checkBoxResult);
                // Commit the edits!
                editor.commit();*/
                return;
            }
        });
        SharedPreferences settings = getSharedPreferences("prefs", MODE_PRIVATE);
        Boolean skipMessage = settings.getBoolean("dontshow",true);
        if (!skipMessage){
        	adb.show();
        }
        super.onResume();
    }
}


