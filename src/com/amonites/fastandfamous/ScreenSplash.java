package com.amonites.fastandfamous;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import com.amonites.daos.MazosDAO;
import com.amonites.clases.Mazo;
import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;

public class ScreenSplash extends Activity {

	private long splashDelay = 5000; //6 segundos

	ArrayList<Mazo> mazos = new ArrayList<Mazo>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_screen_splash);

		TimerTask task = new TimerTask() {
			@Override
			public void run() {


				Intent mainIntent = new Intent().setClass(ScreenSplash.this, MainActivity.class);

				Bundle b = new Bundle();
				b.putSerializable("mazos", mazos);
				mainIntent.putExtras(b);
				startActivity(mainIntent);
				finish();//Destruimos esta activity para prevenit que el usuario retorne aqui presionando el boton Atras.
			}
		};


		//abir, crear mazos y cerrar
		MazosDAO mazosDAO = new MazosDAO(getApplicationContext());
		mazosDAO.open();
		mazos = mazosDAO.getAllMazos();
		mazosDAO.close();

		//crear shared preference dont_show_again(si no esta creada)
		SharedPreferences settings = getSharedPreferences("prefs", MODE_PRIVATE);
		if (!settings.contains("dontshow")){
			settings.edit().putBoolean("dontshow", false).commit();
		}

		Timer timer = new Timer();
		timer.schedule(task, splashDelay);//Pasado los 6 segundos dispara la tarea
	}

}
