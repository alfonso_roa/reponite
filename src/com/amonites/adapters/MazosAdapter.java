package com.amonites.adapters;

import java.util.List;

import android.R.color;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.amonites.clases.Mazo;
import com.amonites.fastandfamous.R;


public class MazosAdapter extends ArrayAdapter<Mazo> {
	private LayoutInflater inflater=null;
	private int resource;
	private Context context;

	public MazosAdapter(Context context, int _resource, List<Mazo> _items) {
		super(context, _resource, _items);
		resource = _resource;
		this.context = context;
		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	static class MazoHolder {
		TextView titulo;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View vi=convertView;
		MazoHolder holder;
		if (convertView == null) {
			vi = inflater.inflate(resource, parent, false);
			holder=new MazoHolder();
			holder.titulo = (TextView)vi.findViewById(R.id.conf_mazo_titulo);
			vi.setTag(holder);
		} else {
			holder=(MazoHolder)vi.getTag();
		}

		Mazo item = getItem(position);  

		holder.titulo.setText(item.getNombre());
		if (!item.isOriginal()){
			holder.titulo.setTextColor(Color.BLUE);
		} else {
			holder.titulo.setTextColor(Color.WHITE);
		}

		holder.titulo.setTag(position);

		return vi;
	}

}