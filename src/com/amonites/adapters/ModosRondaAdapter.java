package com.amonites.adapters;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.amonites.clases.Ronda;
import com.amonites.fastandfamous.R;


public class ModosRondaAdapter extends ArrayAdapter<Ronda> {
	private LayoutInflater inflater=null;

	public ModosRondaAdapter(Activity _activity, int _resource, List<Ronda> _items) {
		super(_activity, _resource, _items);
		inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	static class RondaHolder {
		TextView nombre;
		TextView tiempo;
		Button incrementar;
		Button decrementar;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View vi=convertView;
		RondaHolder holder;
		if (convertView == null) {
			vi = inflater.inflate(R.layout.elemento_ronda, parent, false);
			holder=new RondaHolder();
			holder.nombre = (TextView)vi.findViewById(R.id.textoModoRonda);
			holder.tiempo = (TextView)vi.findViewById(R.id.tiempo_ronda);
			holder.incrementar = (Button)vi.findViewById(R.id.sumar_tiempo_ronda);
			holder.decrementar = (Button)vi.findViewById(R.id.restar_tiempo_ronda);
			vi.setTag(holder);
		} else {
			holder=(RondaHolder)vi.getTag();
		}

		Ronda item = getItem(position);  

		holder.nombre.setText(item.nombre);
		holder.tiempo.setText(item.getTimeWithFormat());

		holder.nombre.setTag(position);
		holder.tiempo.setTag("tiempo");

		return vi;
	}

}