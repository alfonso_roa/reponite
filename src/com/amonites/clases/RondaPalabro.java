package com.amonites.clases;

public class RondaPalabro extends Ronda {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5567918351015907036L;

	public RondaPalabro(int t, int pc) {
		super(t, pc);
		this.nombre = "Palabro";
	}
}
