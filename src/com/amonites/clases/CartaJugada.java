package com.amonites.clases;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class CartaJugada.
 */
public class CartaJugada implements Serializable{
	
	/** objeto que indica si una carta se ha acertado o no. */
	private static final long serialVersionUID = 6030036782740842956L;
	
	/** The carta. */
	public Carta carta;
	
	/** The acertada. */
	public boolean acertada;
}