package com.amonites.clases;

import android.annotation.SuppressLint;
import android.content.Context;

import java.io.Serializable;

/**
 * The Class Carta.
 */
@SuppressLint("DefaultLocale")
public class Carta  implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3711487252504085781L;

	/** The personaje. */
	private String personaje;

	/** The dificultad. */
	private int dificultad;
	//evaluar
	//static int TIPOHISTORICO = 1;
	/** The categoria. */
	private int categoria;

	//a�adido por comodidad para manejo desde bd
	/** The id. */
	private int id;

	/** The mazo_original. */
	private int mazo_original;

	/**
	 * Gets the personaje.
	 *
	 * @return the personaje
	 */
	public String getPersonaje() {
		return personaje;
	}

	/**
	 * Sets the personaje.
	 *
	 * @param personaje the new personaje
	 */
	public void setPersonaje(String personaje) {
		this.personaje = personaje;
	}

	/**
	 * Gets the dificultad.
	 *
	 * @return the dificultad
	 */
	public int getDificultad() {
		return dificultad;
	}

	/**
	 * Sets the dificultad.
	 *
	 * @param dificultad the new dificultad
	 */
	public void setDificultad(int dificultad) {
		this.dificultad = dificultad;
	}

	/**
	 * Gets the categoria.
	 *
	 * @return the categoria
	 */
	public int getCategoria() {
		return categoria;
	}

	/**
	 * Sets the categoria.
	 *
	 * @param categoria the new categoria
	 */
	public void setCategoria(int categoria) {
		this.categoria = categoria;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(int id) {
		this.id = id;
	}

	public int getMazo_original() {
		return mazo_original;
	}

	public void setMazo_original(int mazo_original) {
		this.mazo_original = mazo_original;
	}

	/**
	 * Instantiates a new carta.
	 *
	 * @param p the personaje
	 * @param d the dificultad
	 * @param t the categoria
	 */
	public Carta (String p, int d, int t){
		personaje = p;
		dificultad = d;
		categoria = t;
	}

	/**
	 * Instantiates a new carta.
	 *
	 * @param id the id
	 * @param personaje the personaje
	 * @param categoria the categoria
	 * @param dificultad the dificultad
	 * @param mazo_original the mazo_original al que pertenece
	 */
	public Carta(int id, String personaje, int categoria, int dificultad, int mazo_original) {
		super();
		this.personaje = personaje;
		this.dificultad = dificultad;
		this.categoria = categoria;
		this.id = id;
		this.setMazo_original(mazo_original);
	}

	/**
	 * Instantiates a new carta.
	 *
	 * @param id the id
	 * @param personaje the personaje
	 * @param categoria the categoria
	 * @param dificultad the dificultad
	 * @param mazo_original the mazo_original al que pertenece
	 */
	public Carta() {
		super();
	}

	@Override
	public boolean equals(Object object) {
		boolean sameSame = false;

		if (object != null && object instanceof Carta)
		{
			String nombre = ((Carta) object).personaje.toLowerCase();
			sameSame = this.personaje.toLowerCase().equals(nombre);
		}

		return sameSame;
	}

	@Override
	public int hashCode() {
		int result = 17;

		// Include a hash for each field.
		result = 31 * result + personaje.toLowerCase().hashCode();
		return result;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return personaje;
	}


}
