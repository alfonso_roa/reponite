package com.amonites.clases;

import java.io.Serializable;

public class Scoreboard implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7182917740926487411L;
	private int[][] puntuaciones;

	public Scoreboard(int parejas, int numrondas) {
		puntuaciones = new int[numrondas][parejas];
		for(int[] ronda:puntuaciones){
			for(@SuppressWarnings("unused") int puntuacion:ronda){
				puntuacion=0;
			}
		}
	}

	public void addPuntuacion(int cantidad, int ronda, int pareja){
		puntuaciones[ronda][pareja]+=cantidad;
	}
	
	public int getPuntuacion(int ronda, int pareja){
		return puntuaciones[ronda][pareja];
	}
	
	public int getPuntuacionTotal(int pareja){
		int conta = 0;
		for (int i=0; i<puntuaciones.length; i++){
			conta+=puntuaciones[i][pareja];
		}
		return conta;
	}
}
