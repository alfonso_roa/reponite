package com.amonites.clases;

import android.content.Context;

import com.amonites.clases.Mazo.MazoRonda;
import com.amonites.elementosGraficos.CartasView.AccionCartas;

public class CartasViewAdapter{
	private Context context;
	private int resource;
	private MazoRonda mazoRonda;
	private AccionCartas accion;
	public CartasViewAdapter(Context context, int _resource, MazoRonda mazoRonda, AccionCartas accion){
		this.context=context;
		this.resource=_resource;
		this.mazoRonda=mazoRonda;
		this.accion = accion;
	}
	public Context getContext() {
		return context;
	}
	public int getResource() {
		return resource;
	}
	public MazoRonda getMazoRonda() {
		return mazoRonda;
	}
	public AccionCartas getAccionCartas(){
		return accion;
	}
}