package com.amonites.clases;

import java.util.List;

import com.amonites.fastandfamous.R;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class RevisionCartasAdapter extends ArrayAdapter<CartaJugada> {
	private LayoutInflater inflater=null;
	private Context context;
	
	
	public RevisionCartasAdapter(Context context, int _resource, List<CartaJugada> _items) {
        super(context, _resource, _items);
        this.context = context;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

	static class CartaHolder {
		TextView nombre_carta;
		TextView carta_acertada;
		TextView carta_fallada;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View vi=convertView;
	    CartaHolder holder;
	    if (convertView == null) {
	        vi = inflater.inflate(R.layout.elemento_revision, parent, false);
	        holder=new CartaHolder();
	        holder.nombre_carta = (TextView)vi.findViewById(R.id.nombre_carta_revision);
	        holder.carta_acertada = (TextView)vi.findViewById(R.id.carta_revision_ok);
	        holder.carta_fallada = (TextView)vi.findViewById(R.id.carta_revision_ko);
	        vi.setTag(holder);
	    } else {
	        holder=(CartaHolder)vi.getTag();
	    }
	    
	    CartaJugada item = getItem(position);
	    
	    holder.nombre_carta.setText(item.carta.getPersonaje());
	    if (item.acertada) {
			holder.carta_acertada.setText("ok");
			holder.carta_fallada.setText("");
		} else {
			holder.carta_fallada.setText("ko");
			holder.carta_acertada.setText("");
		}
	    
	    return vi;
	}

	
}
