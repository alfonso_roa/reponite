package com.amonites.clases;

import java.io.Serializable;

public abstract class Ronda implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5632385181809078326L;
	public int time;
	public String nombre;
	private int pasar_cartas;
	private static int TIEMPO = 15;
	
	public Ronda (int t, int pc){
		time = t;
	    setPasar_cartas(pc);
	}
	
	public Ronda (){
		time = 30;
	    setPasar_cartas(2);
	}
	
	@Override
	public String toString(){
		return nombre;
	}

	public void incrementarTiempo() {
		// TODO Auto-generated method stub
		time+=TIEMPO;
		
	}

	public void decrementarTiempo() {
		// TODO Auto-generated method stub
		if (time>0){
			time-=TIEMPO;
		}
		
	}
	
	public String getTimeWithFormat(){
		int min = time/60;
		int seg = time % 60;

		return min+":"+(seg<10?"0"+seg:seg);
	}

	public int getPasar_cartas() {
		return pasar_cartas;
	}

	public void setPasar_cartas(int pasar_cartas) {
		this.pasar_cartas = pasar_cartas;
	}
	
	
}
