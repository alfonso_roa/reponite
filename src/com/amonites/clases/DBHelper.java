package com.amonites.clases;

import java.lang.reflect.Field;

import com.amonites.daos.MazosDAO;
import com.amonites.fastandfamous.R;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "FastAndFamousDB";

	// Mazos table name
	public static final String TABLA_MAZOS = "mazos";

	// Mazos Table Columns names
	public static final String M_0_KEY_ID = "_id";
	public static final String M_1_NOMBRE = "nombre";
	public static final String M_2_TIPO = "tipo";
	public static final String M_3_ORIGINAL = "original";

	//Cartas table name
	public static final String TABLA_CARTAS= "cartas";

	//Cartas Table Columns Names
	public static final String C_0_KEY_ID = "_id";
	public static final String C_1_PERSONAJE = "personaje";
	public static final String C_2_CATEGORIA = "categoria";
	public static final String C_3_MAZO_ORIGINAL = "mazo_original";
	public static final String C_4_DIFICULTAD = "dificultad";


	//MAZO_CARTAS Table Name
	public static final String TABLA_MAZO_CARTAS= "mazo_cartas";

	//Mazo Cartas Tabla Columns Names
	public static final String MC_0_FKEYM_ID = "mazo_id";
	public static final String MC_1_FKEYC_ID = "carta_id";


	public DBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);

	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		// Execute create table SQL
		String create_table_mazos =  "CREATE TABLE " + TABLA_MAZOS + " (" + M_0_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " 
				+ M_1_NOMBRE + " TEXT NOT NULL, " 
				+ M_2_TIPO   + " INTEGER NOT NULL, " 
				+ M_3_ORIGINAL   + " INTEGER NOT NULL, " 
				+ "UNIQUE(" + M_1_NOMBRE + ") ON CONFLICT ROLLBACK"
				+ ");";
		db.execSQL(create_table_mazos);

		String create_table_cartas = "CREATE TABLE " + TABLA_CARTAS + " (" + C_0_KEY_ID    + " INTEGER PRIMARY KEY AUTOINCREMENT, " 
				+ C_1_PERSONAJE     + " TEXT NOT NULL, " 
				+ C_2_CATEGORIA     + " INTEGER NOT NULL, " 
				+ C_3_MAZO_ORIGINAL + " INTEGER NOT NULL, " 
				+ C_4_DIFICULTAD    + " INTEGER, " 
				+ "UNIQUE(" + C_1_PERSONAJE + ") ON CONFLICT ROLLBACK"
				+ ");";

		db.execSQL(create_table_cartas);

		String create_table_mazo_cartas = "CREATE TABLE " + TABLA_MAZO_CARTAS + " (" + MC_0_FKEYM_ID + " INTEGER NOT NULL, " 
				+ MC_1_FKEYC_ID + " INTEGER NOT NULL, " 
				+ "UNIQUE(" + MC_0_FKEYM_ID + "," + MC_1_FKEYC_ID + ") ON CONFLICT ROLLBACK, "
				+ " FOREIGN KEY ("+MC_0_FKEYM_ID+") REFERENCES "+TABLA_MAZOS+" ("+M_0_KEY_ID+"), "
				+ " FOREIGN KEY ("+MC_1_FKEYC_ID+") REFERENCES "+TABLA_CARTAS+" ("+C_0_KEY_ID+"));" ;


		db.execSQL(create_table_mazo_cartas);

		/*//Insertar mazos y cartas iniciales
		db.execSQL("INSERT INTO " + TABLA_MAZOS + " ("+ M_1_NOMBRE + ", " + M_2_TIPO + ") " +
				"VALUES ('mazo inicial', 0);");


		for (int i = 1; i <= 40; i++){

			db.execSQL ("INSERT INTO cartas ("+ C_1_PERSONAJE + ", " + C_2_CATEGORIA + ", "	+ C_3_MAZO_ORIGINAL + ", " + C_4_DIFICULTAD + ") " +
					"VALUES ('personaje"+i+"',1,1,1);");

			db.execSQL ("INSERT INTO mazo_cartas (mazo_id,carta_id) " +
					"VALUES (1,"+ i +");");
		}

		db.execSQL("INSERT INTO " + TABLA_MAZOS + " ("+ M_1_NOMBRE + ", " + M_2_TIPO + ") " +
				"VALUES ('mazo chachi', 0);");

		for (int i = 41; i <= 80; i++){

			db.execSQL ("INSERT INTO cartas (personaje, categoria, mazo_original, dificultad) " +
					"VALUES ('personaje"+ i +"',1,2,1);");

			db.execSQL ("INSERT INTO mazo_cartas (mazo_id,carta_id) " +
					"VALUES (2,"+ i +");");
		}*/


	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {

		switch (arg1+1){
		case 0:
		case 1:addNewMazo(db, "sw");
			addNewMazo(db, "indiana");
		if (arg2 ==1){break;};
		case 2:if (arg2==2){break;};
		default:break;
		}

	}

	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion){
		// DROP table
		db.execSQL("DROP TABLE IF EXISTS mazos");
		db.execSQL("DROP TABLE IF EXISTS cartas");
		db.execSQL("DROP TABLE IF EXISTS mazo_cartas");
		// Recreate table
		onCreate(db);
		onUpgrade(db,0,DATABASE_VERSION);
	}

	
	private void addNewMazo(SQLiteDatabase db,String prefix){
		Field[] nombres = R.string.class.getDeclaredFields();
		Long id = 0l;
		for (Field campo:nombres){
			if (campo.getName().equals(prefix+"_nombre")){
				int intnombre = 0;
				try {
					intnombre = campo.getInt(campo);
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				ContentValues valoresMazo = new ContentValues();
				valoresMazo.put(M_1_NOMBRE, Integer.toString(intnombre));
				valoresMazo.put(M_2_TIPO, 0);
				valoresMazo.put(M_3_ORIGINAL, MazosDAO.ORIGINAL_MAZO);
				id = db.insert(TABLA_MAZOS, null, valoresMazo);
			}
		}

		for (Field campo:nombres){
			String nombre_campo = campo.getName();
			if (prefix.length()<nombre_campo.length() && nombre_campo.substring(0, prefix.length()).equals(prefix) && !(nombre_campo.equals(prefix+"_nombre"))){
				int intnombre = 0;
				try {
					intnombre= campo.getInt(campo);
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				ContentValues valorCarta = new ContentValues();
				valorCarta.put(C_1_PERSONAJE, Integer.toString(intnombre));
				valorCarta.put(C_2_CATEGORIA, 0);
				valorCarta.put(C_3_MAZO_ORIGINAL, id);
				valorCarta.put(C_4_DIFICULTAD, 0);
				Long idc = db.insert(TABLA_CARTAS, null, valorCarta);
				ContentValues valorRelacion = new ContentValues();
				valorRelacion.put(MC_1_FKEYC_ID, idc);
				valorRelacion.put(MC_0_FKEYM_ID, id);
				db.insert(TABLA_MAZO_CARTAS, null, valorRelacion);
			}
		}
	}
}
