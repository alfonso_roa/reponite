package com.amonites.clases;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import com.amonites.clases.Mazo.MazoRonda;
import com.amonites.excepciones.NotEnoughCardsException;

// TODO: Auto-generated Javadoc
/**
 * The Class Partida.
 */
public class Partida implements Serializable {


	private static final long serialVersionUID = -3134805416442545743L;
	private ArrayList<Ronda> rondas; //rondas que se jugar�n
	private Scoreboard puntuacion;
	private ArrayList<Mazo> conjunto_mazos = null;
	private int numCartas = 40;
	private Mazo mazo; //mazo creado para la partida
	private MazoRonda mazo_ronda = null; //mazo ya barajeado y especifico para cada ronda
	private int num_parejas = 2;
	private int ronda_actual = -1;//empiezo en -1 para incrementar la ronda en iniciarRonda();
	private int pareja_actual = 0;

	public Partida (){
		rondas = new ArrayList<Ronda>();
		conjunto_mazos = new ArrayList<Mazo>();
	}

	public void setRondas(ArrayList<Ronda> rs){
		rondas = rs;
	}

	private void setMazo(Mazo m){
		mazo = m;
	}

	public void setNum_parejas(int num_parejas) {
		this.num_parejas = num_parejas;
	}

	public void setConjunto_mazos(ArrayList<Mazo> conjunto_mazos) throws NotEnoughCardsException {
		this.conjunto_mazos = conjunto_mazos;
		checkMazeCorrectness();
	}

	public void setNumCartas(int numCartas) throws NotEnoughCardsException {
		this.numCartas = numCartas;
		checkMazeCorrectness();
	}

	/**
	 * crea un Mazo para jugar una partida a partir de varios mazos
	 * @param mazos
	 * @param numCartas
	 * @return
	 * @throws NotEnoughCardsException 
	 */
	public Mazo creaMazo(ArrayList<Mazo> mazos, int numCartas) throws NotEnoughCardsException{

		//O SE LE PASA UN CLON AL LLAMARLO
		//ArrayList<Mazo> mazosClone = (ArrayList<Mazo>) mazos.clone();

		//Comprobar que haya suficientes cartas entre los mazos
		ArrayList<Carta>cartas_aux = new ArrayList<Carta>();

		int totalCartas = 0;
		for (Mazo m : mazos){
			totalCartas += m.getCartas().size();
		}

		if (totalCartas < numCartas){
			throw new NotEnoughCardsException();
		}
		else{
			//Barajar mazos
			for (Mazo m : mazos){
				m.barajar();
			}
			//crear iteradores de cartas
			ArrayList<Iterator<Carta>> iteradores_cartas= new ArrayList<Iterator<Carta>>();
			for (Mazo m : mazos){
				iteradores_cartas.add(m.getCartas().iterator());
			}
			//seleccionar cartas
			//mientras no se llegue a las cartas que ha dicho el usr
			for (int i = 0; i < numCartas;){
				//para cada iterador de cartas
				for (Iterator<Carta>it : iteradores_cartas){
					//si hay mas cartas en el mazo, y no nos hemos pasado aun de las cartas
					if(it.hasNext() && i < numCartas ){
						cartas_aux.add(it.next());
						i++;
					}
				}
			}
		}	
		return new Mazo("mazo",cartas_aux);
	}

	private Mazo creaMazoAleatorio(){

		//Comprobar que haya suficientes cartas entre los mazos
		Mazo auxiliar = new Mazo("jander");

		for (Mazo m : conjunto_mazos){
			auxiliar.mergeMazos(m);
		}
		//Barajar mazos
		return auxiliar.seleccionAleatoria(numCartas);
	}

	/**
	 * Check maze correctness.
	 *
	 * @throws NotEnoughCardsException the not enough cards exception
	 */
	private void checkMazeCorrectness() throws NotEnoughCardsException{

		//Comprobar que haya suficientes cartas entre los mazos
		Mazo auxiliar = new Mazo("jander");

		for (Mazo m : conjunto_mazos){
			auxiliar.mergeMazos(m);
		}

		if (auxiliar.cantidadCartas() < numCartas){
			throw new NotEnoughCardsException();
		}
		else{
			//Barajar mazos
			setMazo(auxiliar.seleccionAleatoria(numCartas));
		}
	}

	/***************************************
	 * metodo que se lanza cuando se tiene todos los componentes preparados
	 * @throws NotEnoughCardsException 
	 */

	public void iniciarPartida() throws NotEnoughCardsException{
		checkMazeCorrectness();
		puntuacion = new Scoreboard(num_parejas, rondas.size()); //aqui tendria que ir la de todos los jugadores
		pareja_actual = (int) (Math.random()*num_parejas);
		mazo = creaMazoAleatorio();
	}

	/**************************
	 * 
	 */

	public void iniciarRonda(){
		ronda_actual++;
		mazo_ronda = mazo.barajar();
	}

	public void siguienteTurno(){
		pareja_actual = (pareja_actual+1) % num_parejas;
		mazo_ronda.iniciarTurno();
	}

	public int getPareja_actual(){
		return pareja_actual;
	}

	public int getNum_pareja(){
		return num_parejas;
	}

	public ArrayList<Mazo> getConjunto_mazos() {
		return conjunto_mazos;
	}

	public int getNumCartas() {
		return numCartas;
	}

	public MazoRonda getMazoActual(){
		return mazo_ronda;
	}

	public Ronda getRondaActual(){
		return rondas.get(ronda_actual);
	}

	public int getNumero_ronda_actual(){
		return ronda_actual;
	}

	public Scoreboard getScoreboard(){
		return puntuacion;
	}

	public boolean isLastRound(){
		return rondas.size() == ronda_actual+1;
	}
}
