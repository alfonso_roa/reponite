package com.amonites.clases;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import com.amonites.excepciones.CantUseFirstCardException;

public class Mazo  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4574088179287223460L;
	private String nombre;
	private ArrayList<Carta> cartas;
	private int tipo;
	private boolean original = false;

	//a�adido por comidad en manejo de la base de datos
	private long id = -666;

	public Mazo(String nombre, int tipo, boolean original, ArrayList<Carta> cartas ) {
		super();
		this.nombre = nombre;
		this.cartas = cartas;
		this.tipo = tipo;
		this.id = -666;
		this.original = original;
	}

	public Mazo(int id, String nombre, int tipo, boolean original, ArrayList<Carta> cartas ) {
		super();
		this.nombre = nombre;
		this.cartas = cartas;
		this.tipo = tipo;
		this.id = id;
		this.original = original;
	}

	public boolean isOriginal() {
		return original;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public long getId() {
		return id;
	}

	public void setId(long result) {
		this.id = result;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ArrayList<Carta> getCartas() {
		return cartas;
	}

	public void setCartas(ArrayList<Carta> cartas) {
		this.cartas = cartas;
	}

	public void mergeMazos(Mazo mazo){
		for (Carta c:mazo.cartas){
			this.aniadir_carta(c);
		}
	}

	public Mazo (String n){	
		nombre = n;
		cartas = new ArrayList<Carta>();
	}

	public Mazo (String n, ArrayList<Carta> c){
		nombre = n;
		cartas = c;
	}

	public void aniadir_carta(Carta c){
		if (!cartas.contains(c)){
			cartas.add(c);
		}
	}

	public Mazo seleccionAleatoria(int n){
		ArrayList<Carta> cartas_barajadas = new ArrayList<Carta>(cartas);
		Collections.shuffle(cartas_barajadas,new Random(System.nanoTime()));
		ArrayList<Carta> seleccionadas = new ArrayList<Carta>();
		for (int i=0; i<n; i++){
			seleccionadas.add(cartas_barajadas.get(i));
		}
		return new Mazo("seleccionadas",seleccionadas);
	}

	public MazoRonda barajar(){
		ArrayList<Carta> cartas_barajadas = new ArrayList<Carta>(cartas);
		Collections.shuffle(cartas_barajadas,new Random(System.nanoTime()));
		return new MazoRonda(cartas_barajadas);
	}

	public int cantidadCartas(){
		return cartas.size();
	}

	public String toString(){
		return nombre;
	}

	public class MazoRonda implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = -2929077334302843243L;
		private ArrayList<Carta> cartas;
		private Carta carta_actual;
		private Carta carta_primera;

		public MazoRonda(ArrayList<Carta> cartas){
			this.cartas = cartas;
			carta_actual = this.cartas.get(0);
		}

		public void iniciarTurno(){
			carta_primera = carta_actual;
		}

		public void eliminarCarta(Carta c){
			if (carta_actual==c && cartas.size()>1){
				carta_actual = cartas.get((1+cartas.indexOf(carta_actual)) % cartas.size());
			}
			cartas.remove(c);
		}

		public Carta getCarta_actual(){
			return carta_actual;
		}

		public void pasarCarta() throws CantUseFirstCardException{
			int posicion = 1+cartas.indexOf(carta_actual);
			Carta c = cartas.get(posicion % cartas.size());
			if (c!=carta_primera){
				carta_actual = c;
			}
			else{
				throw new CantUseFirstCardException();
			}
		}

		public boolean quedanCartas() {
			return cartas.size()!=0;
		}
	}
}
