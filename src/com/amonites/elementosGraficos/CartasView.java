package com.amonites.elementosGraficos;

import com.amonites.clases.CartasViewAdapter;
import com.amonites.excepciones.CantUseFirstCardException;
import com.amonites.fastandfamous.R;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.view.animation.Animation.AnimationListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CartasView extends RelativeLayout {

	private CartasViewAdapter adaptador;
	private Context contexto;
	private RelativeLayout cartaactual = null;
	private RelativeLayout cartaoculta = null;
	private Typeface tf;
	private String fuente = "fonts/StreetwiseBuddy.ttf";
	private class OnCartaTouchListener implements OnTouchListener {

		float mDownX;
		private int mSwipeSlop = -1;
		private boolean mAnimating;
		private boolean mSwiping;
		private int SWIPE_DURATION = 500;

		@SuppressLint("NewApi")
		@Override
		public boolean onTouch(final View v, MotionEvent event) {
			if (mSwipeSlop < 0) {
				mSwipeSlop = ViewConfiguration.get(contexto).
						getScaledTouchSlop();
			}
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				if (mAnimating) {
					// Multi-item swipes not handled
					return true;
				}
				mDownX = event.getX();
				break;
			case MotionEvent.ACTION_CANCEL:
				setSwipePosition(v, 0);
				break;
			case MotionEvent.ACTION_MOVE:
			{
				if (mAnimating) {
					return true;
				}
				float x = event.getX();
				if (isRuntimePostGingerbread()) {
					x += v.getTranslationX();
				}
				float deltaX = x - mDownX;

				float deltaXAbs = Math.abs(deltaX);
				if (!mSwiping) {
					if (deltaXAbs > mSwipeSlop) {
						mSwiping = true;
					}
				}
				if (mSwiping) {
					setSwipePosition(v, deltaX);
				}
			}
			break;
			case MotionEvent.ACTION_UP:
			{
				if (mAnimating) {
					return true;
				}
				// User let go - figure out whether to animate the view out, or back into place
				if (mSwiping) {
					float x = event.getX();
					if (isRuntimePostGingerbread()) {
						x += v.getTranslationX();
					}
					float deltaX = x - mDownX;
					float deltaXAbs = Math.abs(deltaX);
					float fractionCovered;
					float endX;
					final boolean remove;
					if (deltaXAbs > v.getWidth() / 4) {
						// Greater than a quarter of the width - animate it out
						fractionCovered = deltaXAbs / v.getWidth();
						endX = deltaX < 0 ? -v.getWidth() : v.getWidth();
						remove = true;
						if(deltaX>0){
							adaptador.getAccionCartas().cartaALaDerecha();
						} else {
							adaptador.getAccionCartas().cartaALaIzquierda();
						}
					} else {
						// Not far enough - animate it back
						fractionCovered = 1 - (deltaXAbs / v.getWidth());
						endX = 0;
						remove = false;
					}
					// Animate position and alpha
					long duration = (int) ((1 - fractionCovered) * SWIPE_DURATION);
					animateSwipe(v, endX, duration, remove);
				} else {
				}
			}
			break;
			default: 
				return false;
			}
			return true;
		}

		private float mCurrentX;
		private float mCurrentAlpha;

		@SuppressLint("NewApi")
		private void setSwipePosition(View view, float deltaX) {
			float fraction = Math.abs(deltaX) / view.getWidth();
			if (isRuntimePostGingerbread()) {
				view.setTranslationX(deltaX);
				view.setAlpha(1 - fraction);
			} else {
				// Hello, Gingerbread!
				TranslateAnimation swipeAnim = new TranslateAnimation(deltaX, deltaX, 0, 0);
				mCurrentX = deltaX;
				mCurrentAlpha = (1 - fraction);
				AlphaAnimation alphaAnim = new AlphaAnimation(mCurrentAlpha, mCurrentAlpha);
				AnimationSet set = new AnimationSet(true);
				set.addAnimation(swipeAnim);
				set.addAnimation(alphaAnim);
				set.setFillAfter(true);
				set.setFillEnabled(true);
				view.startAnimation(set);
			}
		}

		/**
		 * Returns true if the current runtime is Honeycomb or later
		 */
		private boolean isRuntimePostGingerbread() {
			return android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB;
		}

		/**
		 * Animates a swipe of the item either back into place or out of the listview container.
		 * NOTE: This is a simplified version of swipe behavior, for the purposes of this demo
		 * about animation. A real version should use velocity (via the VelocityTracker class)
		 * to send the item off or back at an appropriate speed.
		 */
		@SuppressLint("NewApi")
		private void animateSwipe(final View view, float endX, long duration, final boolean remove) {
			mAnimating = true;
			view.setEnabled(false);
			if (isRuntimePostGingerbread()) {
				view.animate().setDuration(duration).
				alpha(remove ? 0 : 1).translationX(endX).
				setListener(new AnimatorListenerAdapter() {
					@Override
					public void onAnimationEnd(Animator animation) {
						// Restore animated values
						view.setAlpha(1);
						view.setTranslationX(0);
						mSwiping = false;
						mAnimating = false;
						view.setEnabled(true);
					}
				});
			} else {
				TranslateAnimation swipeAnim = new TranslateAnimation(mCurrentX, endX, 0, 0);
				AlphaAnimation alphaAnim = new AlphaAnimation(mCurrentAlpha, remove ? 0 : 1);
				AnimationSet set = new AnimationSet(true);
				set.addAnimation(swipeAnim);
				set.addAnimation(alphaAnim);
				set.setDuration(duration);
				view.startAnimation(set);
				setAnimationEndAction(set, new Runnable() {
					@Override
					public void run() {
						//if (remove) {
						//animateOtherViews(mListView, view);
						//} else {
						mSwiping = false;
						mAnimating = false;
						view.setEnabled(true);
						//}
					}
				});
			}

		}

		@SuppressLint("NewApi")
		private void setAnimatorEndAction(Animator animator, final Runnable endAction) {
			if (endAction != null) {
				animator.addListener(new AnimatorListenerAdapter() {
					@Override
					public void onAnimationEnd(Animator animation) {
						endAction.run();
					}
				});
			}
		}

		private void setAnimationEndAction(Animation animation, final Runnable endAction) {
			if (endAction != null) {
				animation.setAnimationListener(new AnimationListenerAdapter() {
					@Override
					public void onAnimationEnd(Animation animation) {
						endAction.run();
					}
				});
			}
		}

		/**
		 * Utility, to avoid having to implement every method in AnimationListener in
		 * every implementation class
		 */
		class AnimationListenerAdapter implements AnimationListener {

			@Override
			public void onAnimationEnd(Animation animation) {
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationStart(Animation animation) {
			}
		}
	};

	public CartasView(Context context) {
		super(context);
		this.contexto=context;
	}

	public CartasView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.contexto=context;
	}

	public void setAdapter(CartasViewAdapter adaptador){
		tf = Typeface.createFromAsset(this.contexto.getAssets(), fuente);
		this.adaptador = adaptador;
		cartaactual = new RelativeLayout(contexto);
		inflate(contexto, R.layout.carta, cartaactual);
		cartaoculta = new RelativeLayout(contexto);
		inflate(contexto, R.layout.carta, cartaoculta);
		TextView nombre_personaje = (TextView) cartaactual.findViewById(R.id.text_nombre_personaje);
		nombre_personaje.setText(this.adaptador.getMazoRonda().getCarta_actual().getPersonaje());
		nombre_personaje.setTypeface(tf);
		nombre_personaje = (TextView) cartaoculta.findViewById(R.id.text_nombre_personaje);
		nombre_personaje.setTypeface(tf);
		cartaactual.setOnTouchListener(new OnCartaTouchListener());
		cartaoculta.setOnTouchListener(new OnCartaTouchListener());
		cartaoculta.setVisibility(View.INVISIBLE);
		//cartaoculta.setAlpha(0);
		cartaoculta.setEnabled(false);
		this.addView(cartaactual);
		this.addView(cartaoculta);
	}

	public void showNextCard()  throws CantUseFirstCardException{
		adaptador.getMazoRonda().pasarCarta();
		intercambio();
		TextView nombre_personaje = (TextView) cartaactual.findViewById(R.id.text_nombre_personaje);
		nombre_personaje.setText(adaptador.getMazoRonda().getCarta_actual().getPersonaje());
		bringChildToFront(cartaactual);
		cartaactual.setVisibility(View.VISIBLE);
		if (isRuntimePostGingerbread()){
			cartaactual.setTranslationY(-150);
			cartaactual.setAlpha(0);
		}
		animateNewCard(cartaactual, 0, -150);cartaactual.setEnabled(true);
	}

	public void intercambio(){
		RelativeLayout aux = cartaactual;
		cartaactual=cartaoculta;
		cartaoculta=aux;
		cartaoculta.setVisibility(View.INVISIBLE);
		cartaoculta.setEnabled(false);
	}

	@SuppressLint("NewApi")
	private void animateNewCard(final View view, float endY, float mCurrentY) {
		view.setEnabled(false);
		long duration = 300;
		if (isRuntimePostGingerbread()) {
			view.animate().setDuration(duration ).
			alpha(1).translationY(endY).
			setListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					// Restore animated values
					view.setAlpha(1);
					view.setTranslationY(0);
					view.setEnabled(true);
				}
			});
		} else {
			TranslateAnimation swipeAnim = new TranslateAnimation(0, 0, mCurrentY, endY);
			AlphaAnimation alphaAnim = new AlphaAnimation(0, 1);
			AnimationSet set = new AnimationSet(true);
			set.addAnimation(swipeAnim);
			set.addAnimation(alphaAnim);
			set.setDuration(duration);
			view.startAnimation(set);
			setAnimationEndAction(set, new Runnable() {
				@Override
				public void run() {
					view.setEnabled(true);
					//}
				}
			});
		}

	}

	/**
	 * Returns true if the current runtime is Honeycomb or later
	 */
	private boolean isRuntimePostGingerbread() {
		return android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB;
	}

	@SuppressLint("NewApi")
	private void setAnimatorEndAction(Animator animator, final Runnable endAction) {
		if (endAction != null) {
			animator.addListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					endAction.run();
				}
			});
		}
	}

	private void setAnimationEndAction(Animation animation, final Runnable endAction) {
		if (endAction != null) {
			animation.setAnimationListener(new AnimationListenerAdapter() {
				@Override
				public void onAnimationEnd(Animation animation) {
					endAction.run();
				}
			});
		}
	}

	/**
	 * Utility, to avoid having to implement every method in AnimationListener in
	 * every implementation class
	 */
	class AnimationListenerAdapter implements AnimationListener {

		@Override
		public void onAnimationEnd(Animation animation) {
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
		}

		@Override
		public void onAnimationStart(Animation animation) {
		}
	}

	public interface AccionCartas{
		abstract void cartaALaIzquierda();
		abstract void cartaALaDerecha();
	}
}
