package com.amonites.excepciones;

public class NotEnoughCardsException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9197367136136131853L;

	public NotEnoughCardsException () {
		super("No hay suficientes cartas");
	}
}
