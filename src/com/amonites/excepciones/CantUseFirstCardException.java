package com.amonites.excepciones;

public class CantUseFirstCardException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8469941904473558203L;

	public CantUseFirstCardException() {
		super("No se puede rejugar esa carta");
	}

}
