package com.amonites.daos;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.amonites.clases.Carta;
import com.amonites.clases.DBHelper;

public class CartasDAO {

	private SQLiteDatabase database;
	private DBHelper dbHelper;
	private String[] allColumns = { DBHelper.C_0_KEY_ID, DBHelper.C_1_PERSONAJE, DBHelper.C_2_CATEGORIA, DBHelper.C_3_MAZO_ORIGINAL, DBHelper.C_4_DIFICULTAD };
	
	private Context context;

	public CartasDAO(Context context) {
		dbHelper = new DBHelper(context);
		this.context = context;
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	// Añadir metodos CRUD!! (Create, update, remove)

	public Carta addCarta(String personaje, int categoria, int mazo_original, int dificultad){
		ContentValues values = new ContentValues();
		values.put(DBHelper.C_1_PERSONAJE, personaje);
		values.put(DBHelper.C_2_CATEGORIA, categoria);
		values.put(DBHelper.C_3_MAZO_ORIGINAL, mazo_original);
		values.put(DBHelper.C_4_DIFICULTAD, dificultad);
		long insertId = database.insert(DBHelper.TABLA_CARTAS, null, values);
		Cursor cursor = database.query(DBHelper.TABLA_CARTAS,
				allColumns, DBHelper.C_0_KEY_ID + " = " + insertId, null,
				null, null, null);
		cursor.moveToFirst();
		return cursorToCarta(cursor);
	}

	public Carta findCartaById(int id){
		Cursor cursor = database.query(DBHelper.TABLA_CARTAS, allColumns, DBHelper.C_0_KEY_ID+" = "+id, null, null, null, null);
		cursor.moveToFirst();
		return cursorToCarta(cursor);
	}

	//tengo mis dudas de si meterlo aqui o en el CartasDAO, pero de momento va a aqui
	public ArrayList<Carta> buscarCartas (){
		ArrayList<Carta> cartas = new ArrayList<Carta>();

		Cursor cursor = database.query(DBHelper.TABLA_CARTAS,allColumns,null,null,null,null,null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()){
			cartas.add(cursorToCarta(cursor));
			cursor.moveToNext();
		}

		return cartas;
	}
	
	
	public List<Carta> getAllCartas(){
		List<Carta> list = new ArrayList<Carta>();
		Cursor cursor = database.query(DBHelper.TABLA_CARTAS, allColumns, null, null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
		      Carta carta = cursorToCarta(cursor);
		      list.add(carta);
		      cursor.moveToNext();
		    }
		return list;
	}

	public ArrayList<Carta> buscarCartasMazo (int mazo_id){
		ArrayList<Carta> cartas = new ArrayList<Carta>();

		String rawQuery = "SELECT cartas.* FROM "+DBHelper.TABLA_MAZO_CARTAS+" INNER JOIN "+DBHelper.TABLA_CARTAS+" ON "
		+DBHelper.TABLA_MAZO_CARTAS+"."+DBHelper.MC_0_FKEYM_ID+"=? and "
		+DBHelper.TABLA_MAZO_CARTAS+"."+DBHelper.MC_1_FKEYC_ID+" = "+DBHelper.TABLA_CARTAS+"."+DBHelper.C_0_KEY_ID+" ";

		Cursor cursor = database.rawQuery(rawQuery, new String[]{String.valueOf(mazo_id)});
		cursor.moveToFirst();
		while (!cursor.isAfterLast()){
			cartas.add(cursorToCarta(cursor));
			cursor.moveToNext();
		}
		return cartas;
	}
	
	private Carta cursorToCarta(Cursor cursor){
		return new Carta(getIdCarta(cursor),getNombreCarta(cursor),getCategoriaCarta(cursor),getDificultadCarta(cursor),getMazoOriginalCarta(cursor));
	}
	
	private int getIdCarta(Cursor cursor){
		return cursor.getInt(0);
	}
	
	private String getNombreCarta(Cursor cursor){
		int valor = 0;
		String nombre = "";
		String aux = cursor.getString(1);
		try{
			valor = Integer.parseInt(aux);
		} catch (NumberFormatException e){
			nombre = aux;
		}finally {
			if (nombre.equals("")){
				nombre = context.getString(valor);
			}
		}
		return nombre;
	}
	
	private int getCategoriaCarta(Cursor cursor){
		return cursor.getInt(2);
	}

	
	private int getDificultadCarta(Cursor cursor){
		return cursor.getInt(3);
	}
	
	private int getMazoOriginalCarta(Cursor cursor){
		return cursor.getInt(4);
	}
}
