package com.amonites.daos;

import java.util.ArrayList;

import com.amonites.clases.Carta;
import com.amonites.clases.DBHelper;
import com.amonites.clases.Mazo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class MazosDAO {

	private SQLiteDatabase database;
	private DBHelper dbHelper;
	private String[] columnas_mazo = { DBHelper.M_0_KEY_ID, DBHelper.M_1_NOMBRE, DBHelper.M_2_TIPO, DBHelper.M_3_ORIGINAL };
	private Context context;

	static public int ORIGINAL_MAZO = 0;
	static public int CUSTOM_MAZO = 1;

	public MazosDAO(Context context) {
		this.context = context;
		dbHelper = new DBHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getReadableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public long aniadirMazo(Mazo mazo){
		long result = 0;

		ContentValues valoresMazo = new ContentValues();
		valoresMazo.put(DBHelper.M_1_NOMBRE, mazo.getNombre());
		valoresMazo.put(DBHelper.M_2_TIPO, 0);
		valoresMazo.put(DBHelper.M_3_ORIGINAL, mazo.isOriginal()?ORIGINAL_MAZO:CUSTOM_MAZO);
		if (mazo.getId()!=-666){//si es un mazo ya existente
			String[] where = {Long.toString(mazo.getId())};
			database.update(DBHelper.TABLA_MAZOS, valoresMazo, DBHelper.M_0_KEY_ID+" = ?", where);
			database.delete(DBHelper.TABLA_MAZO_CARTAS, DBHelper.MC_0_FKEYM_ID+" = ?", where);
		} else {//si es un mazo nuevo
			result = database.insert(DBHelper.TABLA_MAZOS, null, valoresMazo);
			if (result!=-1){
				mazo.setId(result);
			}
		}

		ArrayList<Carta> cartas = mazo.getCartas();

		for (Carta carta:cartas){
			addCartaToMazo(mazo, carta);
		}

		return result;
	}

	public void borrarMazo(Mazo	mazo){
		String[] where = {Long.toString(mazo.getId())};
		database.delete(DBHelper.TABLA_MAZO_CARTAS, DBHelper.MC_0_FKEYM_ID+" = ?", where);
		database.delete(DBHelper.TABLA_MAZOS, DBHelper.M_0_KEY_ID+" = ?", where);
		//Se le busca por nombre (SACA ID) y se le borra
		//Se borran todas las asociaciones en mazo_cartas
		//Se borran sus carta si el mazo original es este.
	}

	public Mazo buscarMazo(String nombre){
		//Se le pasa un nombre y te busca el mazo y te lo devuelve con sus cartas y todo

		return null;
	}

	public void addCartaToMazo(Mazo mazo, Carta carta){
		ContentValues valorRelacion = new ContentValues();
		valorRelacion.put(DBHelper.MC_0_FKEYM_ID, mazo.getId());
		valorRelacion.put(DBHelper.MC_1_FKEYC_ID, carta.getId());
		database.insert(DBHelper.TABLA_MAZO_CARTAS, null, valorRelacion);
	}
	
	public ArrayList<Mazo> getAllMazos(){

		ArrayList<Mazo> mazos = new ArrayList<Mazo>();

		Cursor cursor = database.query(DBHelper.TABLA_MAZOS, columnas_mazo,null,null,null,null,null);
		cursor.moveToFirst();

		while (!cursor.isAfterLast()){	
			mazos.add(cursorToMazo(cursor));
			cursor.moveToNext();
		}
		return mazos;
	}

	private Mazo cursorToMazo(Cursor cursor){
		CartasDAO cartasDAO = new CartasDAO(context);
		cartasDAO.open();
		ArrayList<Carta> cartas_aux = cartasDAO.buscarCartasMazo(getId(cursor));
		return new Mazo(getId(cursor),getNombre(cursor),getTipo(cursor), isOriginal(cursor),cartas_aux);
	}

	private int getId(Cursor cursor){
		return cursor.getInt(0);
	}

	private String getNombre(Cursor cursor){
		int valor = 0;
		String nombre = "";
		String aux = cursor.getString(1);
		try{
			valor = Integer.parseInt(aux);
		} catch (NumberFormatException e){
			nombre = aux;
		}finally {
			if (nombre.equals("")){
				nombre = context.getString(valor);
			}
		}
		return nombre;
	}

	private int getTipo(Cursor cursor){
		return cursor.getInt(2);
	}

	private boolean isOriginal(Cursor cursor){
		return cursor.getInt(3)==ORIGINAL_MAZO;
	}
}
